### 版本

当前版本> 最新版本

根据TongYao版本，下载对应前端版本。

### 框架描述
TongYaoHtmlAdmin是一款轻便简约的前端框架，原生Html编写的一套前端框架，融洽了Hplus框架、富文本编辑器，还配备了后台TongYao SpringBoot框架，完美运行，内置有各种可靠使用的方法函数。

后台配备了TongYaoSpringBoot框架，该框架配备了Shiro安全集成、用户管理、菜单管理、角色权限管理，还配备了核心业务代码生成器，从而省去大量的环境框架搭建成本，节省了开发工作者的时间，以提高效率。

该框架为前后台分离框架，需配合后端TongYao框架进行操作执行。

![输入图片说明](https://i.loli.net/2020/12/27/orkJdsMI3jGnEga.gif "屏幕截图.png")



### 搭配框架
| 序号 | 名称           | 地址 |
|----|--------------|----|
| 1  | TongYao      |https://gitee.com/Super_TongYao/TongYao.git    |
| 2  | TongYao_Html-admin |https://gitee.com/Super_TongYao/tong-yao_-html-admin.git    |

如果您有更好的想法和建议，请联系邮箱：super_tongyao@163.com，联系我！
### 目录层次介绍

```
Tong-Yao_-Html-Admin
+---css     //样式文件
+---fonts    //字体配置
+---img    //图片位置       
\---js    //脚本文件存放位置
|   homePage.html    //首页
|   login.html    //登录页
|   mainPage.html    //主页
|   roleManager.html    //角色管理页
|   userManager.html    //用户管理页                    
```

### 更新日志

1、初始化前台代码
2、新增日期方法转换格式

### 运行文档
 **1、下载/解压** 


通过克隆/下载，下载地址：https://gitee.com/Super_TongYao/tong-yao_-html-admin.git

下载完或克隆完本项目到本地，并解压。

 **2、打开** 

使用任意文档编辑工具等相关IDE开发工具导入或引入本项目。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/155129_d9f222a9_2100316.png "屏幕截图.png")


 **3、修改配置文件** 

打开目录js下的pubProcessing.js文件，并且修改后台地址。（PS：本文档后台框架用的是TongYao框架，地址：https://gitee.com/Super_TongYao/TongYao.git）
修改getServerPath()方法的地址为：http://localhost:80/

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/155705_ebb250dc_2100316.png "屏幕截图.png")

如果此时此刻你的项目运行不了，出现下面等类似跨域问题，请看 [使用nginx解决跨域请求](http://yaoyaoman.cn/blog/archives/105)

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/160135_9c075c28_2100316.png "屏幕截图.png")

 ** 4、运行访问** 
直接本地双击打开login.html页面或者放到nginx里、tomcat里都可以进行访问。

输入账号：admin 密码：123456    下图我是放到nginx里面的

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/161313_f7b10dba_2100316.png "屏幕截图.png")

登录后的主页

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/162655_b453e521_2100316.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/162814_aff6b39d_2100316.png "屏幕截图.png")

本前端框架部分细节页面还需美化，如果你有时间，可以发送邮箱：super_tongyao@163.com，来联系我，获得管理开发权限。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/162741_130892cf_2100316.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/162918_cd7e1a5c_2100316.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/162934_0b21d127_2100316.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/162958_ef01c761_2100316.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/163018_19f6e6b3_2100316.png "屏幕截图.png")

当然更多核心业务可以搭配此框架开发更多功能！ :smile: 

 **博客地址：http://yaoyaoman.cn/blog** 
