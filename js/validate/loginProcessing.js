/*$(function () {
    $("[method='post']").attr("action","javascript:verifyLogin()")
})

function verifyLogin() {
    $("[type='submit']").text("正在登录")
    $("[type='submit']").attr({"disabled":"disabled"});
    //序列化表单
    var formStr = $('#loginForm').serialize()
    //处理表单控件的中文乱码
    resultData = decodeURIComponent(formStr,true);
    $.post(getServerPath()+"/VerifyLogin",resultData,function (data) {
        if(data.requestCode == 100){
            sessionStorage.setItem("requestMap",JSON.stringify(data.requestMap));
            $("[method='post']").attr("action","BMainPage.html")
            $("[method='post']").submit();
        }else{
            $("[type='submit']").text("安全登录");
            $("[type='submit']").removeAttr("disabled");//将按钮可用
            alert("账号或密码错误！")
        }
    })
}*/


$(function(){

    //document.getElementById('password').focus().select();
    $("[type=submit]").click(function(){
        var username = $("[name=username]").val();
        var password = $("[name=password]").val();

        if(username == '' || username == null){
            parent.layer.alert('请输入用户名！', {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false;
        }
        if(password == '' || password == null){
            parent.layer.alert('请输入账户密码！', {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false;
        }

        var data = request("post","login?username="+username+"&password="+md5(password),"")
        console.log(data)


        if(data.code != 200){
            parent.layer.alert(data.message, {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false
        }

        sessionStorage.setItem("tongyao-username",data.data.username)
        $("[method=post]").attr("action","mainPage.html")
    })
})


