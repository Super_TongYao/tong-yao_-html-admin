$(function (){
    $("#dataTable").bootstrapTable({
        url:getServerPath()+"/system/sys-role/page?pageNo=1&pageSize=10",
        pagination:true,
        pageSize:10,
        pageList:[2,4,8,10],
        striped:true,
        toolbar: '#toolbar',
        sidePagination: "client",
        pageNumber:1,
        search: true,
        showRefresh: true,
        responseHandler:function(data){
            return data.data.records
        },
        columns:[
            {checkbox:true},
            {field:"id",title:"id",align:"center"},
            {field:"roleName",title:"角色名称",align:"center"},
            {field:"roleDesc",title:"角色详情",align:"center"},
            {field:"roleStatus",title:"是否启用",align:"center"},
            {field:"createTime",title:"创建时间",align:"center"},
            {title:"操作",width:"110px",
                formatter:function (value,row,index){
                    UserName = row.userName;
                    return "   <button type='button' class='btn btn-primary' onclick=queryMenu('"+row.id+"')>\n" +
                        "       <i class='fa fa-tasks'></i>" +
                        "       分配菜单\n" +
                        "    </button>"
                }
            }
        ]
    })

    $("[name=update]").click(function () {
        var options = $("#dataTable").bootstrapTable('getSelections') ;
        if(options.length != 1){
            parent.layer.alert("请选择一行数据！", {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false
        }

        loadData(options[0])
        $("#addRole").modal('show')

    })

    $("[name=save]").click(function () {
        var dataObj = {};
        $.each($("#addInfoForm").serializeArray(),function () {
            dataObj[this.name] = this.value
        })
        if(dataObj.id == ""){
            var data = request("post","system/sys-role/add",dataObj)
            if(data.code != 200){
                parent.layer.alert(data.message, {
                    skin: 'layui-layer-molv', //样式类名
                    shift: 6 ,//动画类型
                    icon:5
                });
                return false
            }
            $(':input','#addInfoForm').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            $("#dataTable").bootstrapTable("refresh",data.data.records);
            $("#addRole").modal('hide')
            parent.layer.alert(data.data, {
                skin: 'layui-layer-molv', //样式类名
                shift: 1 ,//动画类型
                icon:1
            });
            return false
        }
        var data = request("put","system/sys-role/update",dataObj)
        if(data.code != 200){
            parent.layer.alert(data.message, {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false
        }
        $(':input','#addInfoForm').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        $("#dataTable").bootstrapTable("refresh",data.data.records);
        $("#addRole").modal('hide')
        parent.layer.alert(data.data, {
            skin: 'layui-layer-molv', //样式类名
            shift: 1 ,//动画类型
            icon:1
        });

    })

    $("[name=updateRole]").click(function () {
        var dataObj = new Object();
        dataObj.userName = username;
        var data = request("put","system/sys-role/update",dataObj)
        if(data.code != 200){
            parent.layer.alert(data.message, {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false
        }
        parent.layer.alert(data.data, {
            skin: 'layui-layer-molv', //样式类名
            shift: 1 ,//动画类型
            icon:1
        });
        $("#roleModal").modal('hide')
    })

    $("[name=delete]").click(function () {
        var options = $("#dataTable").bootstrapTable('getSelections') ;
        if(options.length == 0){
            parent.layer.alert("请至少选择一行数据！", {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false
        }
        var ids = ""
        for(var i = 0;i < options.length;i++){
            ids = ids + options[i].id+",";
        }
        var data = request("delete","system/sys-role/delete?ids="+ids,"")
        if(data.code != 200){
            parent.layer.alert(data.message, {
                skin: 'layui-layer-molv', //样式类名
                shift: 6 ,//动画类型
                icon:5
            });
            return false
        }
        $("#dataTable").bootstrapTable("refresh",data.data.records);
        parent.layer.alert(data.data, {
            skin: 'layui-layer-molv', //样式类名
            shift: 1 ,//动画类型
            icon:1
        });
    })

})




var roleId = "";
function queryMenu(id){
    roleId = id;
    $("#roleModal").modal("show");
    var data = request("get","system/sys-role-menu/listRoleMenu?roleId="+id,"")
    if(data.code != 200){
        parent.layer.alert(data.message, {
            skin: 'layui-layer-molv', //样式类名
            shift: 6 ,//动画类型
            icon:5
        });
        return false
    }

    /*var dataMenu = [];
    data = data.data;
    for (var i = 0;i<data.length;i++){
        var temp = new Object();
        temp.id = data[i].id
        temp.parentId = data[i].parentId
        temp.text = data[i].menuName
        temp.node = data[i].childrenItem
        dataMenu.push(temp)
    }
    console.log(dataMenu)*/
    $('#searchTree').treeview({
        showCheckbox: true,
        data: data.data,
        onNodeChecked: nodeChecked,
        onNodeUnchecked: nodeUnchecked
    });
}

function doPermissionAllocation(){
    var menuId = "";
    var cks = $("#searchTree").treeview("getChecked");
    $.each(cks,function (i,n) {
        menuId = menuId + "," + n.id;
    })

    var data = request("post","system/sys-role-menu/add?roleId="+roleId+"&menuIds="+menuId,"")
    if(data.code != 200){
        parent.layer.alert(data.message, {
            skin: 'layui-layer-molv', //样式类名
            shift: 6 ,//动画类型
            icon:5
        });
        return false
    }
    parent.layer.alert(data.data, {
        skin: 'layui-layer-molv', //样式类名
        shift: 1 ,//动画类型
        icon:1
    });
    $("#roleModal").modal("hide");
}

var nodeCheckedSilent = false;
function nodeChecked(event, node) {
    if (nodeCheckedSilent) {
        return;
    }
    nodeCheckedSilent = true;
    checkAllParent(node);
    checkAllSon(node);
    nodeCheckedSilent = false;
}

var nodeUncheckedSilent = false;

function nodeUnchecked(event, node) {
    if (nodeUncheckedSilent)
        return;
    nodeUncheckedSilent = true;
    uncheckAllParent(node);
    uncheckAllSon(node);
    nodeUncheckedSilent = false;
}

//选中全部父节点
function checkAllParent(node) {
    $('#searchTree').treeview('checkNode', node.nodeId, {
        silent: true
    });
    var parentNode = $('#searchTree').treeview('getParent', node.nodeId);
    if (!("nodeId" in parentNode)) {
        return;
    } else {
        checkAllParent(parentNode);
    }
}
//取消全部父节点
function uncheckAllParent(node) {
    $('#searchTree').treeview('uncheckNode', node.nodeId, {
        silent: true
    });
    var siblings = $('#searchTree').treeview('getSiblings', node.nodeId);
    var parentNode = $('#searchTree').treeview('getParent', node.nodeId);
    if (!("nodeId" in parentNode)) {
        return;
    }
    var isAllUnchecked = true; //是否全部没选中
    for (var i in siblings) {
        if (siblings[i].state.checked) {
            isAllUnchecked = false;
            break;
        }
    }
    if (isAllUnchecked) {
        uncheckAllParent(parentNode);
    }

}

//级联选中所有子节点
function checkAllSon(node) {
    $('#searchTree').treeview('checkNode', node.nodeId, {
        silent: true
    });
    if (node.nodes != null && node.nodes.length > 0) {
        for (var i in node.nodes) {
            checkAllSon(node.nodes[i]);
        }
    }
}
//级联取消所有子节点
function uncheckAllSon(node) {
    $('#searchTree').treeview('uncheckNode', node.nodeId, {
        silent: true
    });
    if (node.nodes != null && node.nodes.length > 0) {
        for (var i in node.nodes) {
            uncheckAllSon(node.nodes[i]);
        }
    }
}