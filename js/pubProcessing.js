/**
 * @Tite 页面公共使用js
 * @Author 张童瑶
 * @Copyright 张童瑶版权所有，盗版必究。
 * @QQ群：498694317
 */
$(function () {
    var topHref = window.location.href.split("/");
    var pageName = topHref[topHref.length-1];
    if(pageName == "Login.html" || pageName == "Register.html"){}else{
        //验证用户是否登录
        var storageData = JSON.parse(sessionStorage.getItem("requestMap"));
        //if(storageData == "" || storageData == null || storageData == "undefined"){
        //    alert("您还没有登录，没有权限访问。");
        //    window.location.href="Login.html";
        //}
    }

    //公用提示框
    /*$(function (){
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    })*/

    console.log("本项目UI框架有张童瑶版权所有")
})

function getServerPath(){
    return "http://localhost:80/";
}

function request(type,url,dataJson){
    var returnData = null
    if(type == "post"){
        $.ajax({
            url:getServerPath()+url,
            method:'post',
            dataType:'json',
            contentType : 'application/json',
            data:JSON.stringify(dataJson),
            timeout : 6000,
            async:false,//同步请求
            success: function(data){
                //校验登录是否过期
                if(data.code == 401){
                    parent.layer.alert(data.message, {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    },function () {
                        window.location.href = "login.html"
                    });
                }
                returnData = data
            },
            error:function(err){
                parent.layer.alert('获取服务器数据出错了！', {
                    skin: 'layui-layer-molv', //样式类名
                    shift: 6 ,//动画类型
                    icon:5
                });
                console.error("request：请求数据出错，url为路径："+url+"\n错误信息：",err);
                returnData = data
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况
                    ajaxTimeoutTest.abort();
                    parent.layer.alert('网络不稳定，请求超时！', {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    });
                }
            }
        });
    }else if(type == "get"){
        $.ajax({
            url:getServerPath()+url,
            method:'get',
            timeout : 6000,
            async:false,//同步请求
            success: function(data){
                //校验登录是否过期
                if(data.code == 401){
                    parent.layer.alert(data.message, {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    },function () {
                        window.location.href = "login.html"
                    });
                }
                returnData = data
            },
            error:function(err){
                parent.layer.alert('获取服务器数据出错了！', {
                    skin: 'layui-layer-molv', //样式类名
                    shift: 6 ,//动画类型
                    icon:5
                });
                console.error("request：请求数据出错，url为路径："+url+"\n错误信息："+err);
                returnData = data
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况
                    ajaxTimeoutTest.abort();
                    parent.layer.alert('网络不稳定，请求超时！', {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    });
                }
            }
        });
    }else if(type == "put"){
        $.ajax({
            url:getServerPath()+url,
            method:'put',
            dataType:'json',
            contentType : 'application/json',
            data:JSON.stringify(dataJson),
            timeout : 6000,
            async:false,//同步请求
            success: function(data){
                //校验登录是否过期
                if(data.code == 401){
                    parent.layer.alert(data.message, {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    },function () {
                        window.location.href = "login.html"
                    });
                }
                returnData = data
            },
            error:function(err){
                parent.layer.alert('获取服务器数据出错了！', {
                    skin: 'layui-layer-molv', //样式类名
                    shift: 6 ,//动画类型
                    icon:5
                });
                console.error("request：请求数据出错，url为路径："+url+"\n错误信息：",err);
                returnData = data
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况
                    ajaxTimeoutTest.abort();
                    parent.layer.alert('网络不稳定，请求超时！', {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    });
                }
            }
        });
    }else if(type == "delete"){
        $.ajax({
            url:getServerPath()+url,
            method:'delete',
            dataType:'json',
            contentType : 'application/json',
            data:JSON.stringify(dataJson),
            timeout : 6000,
            async:false,//同步请求
            success: function(data){
                //校验登录是否过期
                if(data.code == 401){
                    parent.layer.alert(data.message, {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    },function () {
                        window.location.href = "login.html"
                    });
                }
                returnData = data
            },
            error:function(err){
                parent.layer.alert('获取服务器数据出错了！', {
                    skin: 'layui-layer-molv', //样式类名
                    shift: 6 ,//动画类型
                    icon:5
                });
                console.error("request：请求数据出错，url为路径："+url+"\n错误信息：",err);
                returnData = data
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况
                    ajaxTimeoutTest.abort();
                    parent.layer.alert('网络不稳定，请求超时！', {
                        skin: 'layui-layer-molv', //样式类名
                        shift: 6 ,//动画类型
                        icon:5
                    });
                }
            }
        });
    }else{
        console.error("未识别的请求类型："+type);
        return null;
    }

    return returnData
}

function loadData(jsonStr){
    var obj = jsonStr;
    var key,value,tagName,type,arr;
    for(x in obj){
        key = x;
        value = obj[x];
        $("[name='"+key+"'],[name='"+key+"[]']").each(function(){
            tagName = $(this)[0].tagName;
            type = $(this).attr('type');
            if(tagName=='INPUT'){
                if(type=='radio'){
                    $(this).attr('checked',$(this).val()==value);
                }else if(type=='checkbox'){
                    arr = value.split(',');
                    for(var i =0;i<arr.length;i++){
                        if($(this).val()==arr[i]){
                            $(this).attr('checked',true);
                            break;
                        }
                    }
                }else{
                    $(this).val(value);
                }
            }else if(tagName=='SELECT' || tagName=='TEXTAREA'){
                $(this).val(value);
            }

        });
    }
}

/**
 * @Modify by 张童瑶
 * @Time 2020-11-16 20点48分
 *
 * 新增日期格式公用方法函数
 */

// 编码：encodeURI("张童瑶")
// 结果："%E5%BC%A0%E7%AB%A5%E7%91%B6"

// 解码：decodeURI("%E5%BC%A0%E7%AB%A5%E7%91%B6")
// 结果："张童瑶"

// 对象转数组：JSON.stringify(obj)

/**
 * 获得指定月的开始结束日期
 * 用法：getMonthStartEndDate("2020-01")
 * @param vars
 * @returns {Array}
 */
function getMonthStartEndDate(vars){
    vars = vars.replace("-","");
    var array = [];
    if(vars!=null&&vars!=''){
        var nyYear=vars.slice(0,4);
        var nyMonth=vars.slice(4,vars.length);
        var firstDay = new Date(nyYear,nyMonth-1);
        var lastDay = new Date(new Date(nyYear,nyMonth).valueOf()-60*60*1000*24);
        function datasFormat(d){
            var datetime=d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
            return datetime;
        }
        array[0] = datasFormat(firstDay);
        array[1] = datasFormat(lastDay);
    }
    return array
};


/**
 * 获得本周开始结束日期
 * 用法：getWeekStartEndDate()
 * @returns {Array}
 */
function getWeekStartEndDate() {
    oToday=new Date();
    currentDay=oToday.getDay();
    if(currentDay==0){currentDay=7}
    mondayTime=oToday.getTime()-(currentDay-1)*24*60*60*1000;
    sundayTime=oToday.getTime()+(7-currentDay)*24*60*60*1000;

    //alert("今天是bai"+oToday.getDate()+"号du，星期zhidao"+currentDay+"\r");
    //alert("周一是"+new Date(mondayTime).getDate()+"号，周日是"+new Date(sundayTime).getDate()+"号")
    //alert("周一 "+new Date(mondayTime).toLocaleDateString()+"\n周末答 "+new Date(sundayTime).toLocaleDateString()+" "+new Date(mondayTime).toLocaleTimeString())

    var array = [];
    array[0] = new Date(mondayTime).toLocaleDateString().toString().replaceAll('/','-')
    array[1] = new Date(sundayTime).toLocaleDateString().toString().replaceAll('/','-')
    return array;
}

/**
 * 根据开始结束日期获得中间所有天数
 * 用法：
 *  1、byStartEndDateGetMiddleDate("2020-1-1","2020-1-5")
 *  2、byStartEndDateGetMiddleDate("2020-1","2020-5")
 *  3、byStartEndDateGetMiddleDate("2020","2022")
 * @param startTime 开始日期
 * @param endTime   结束日期
 * @returns {Array}
 */
function byStartEndDateGetMiddleDay(startDate,endDate){
    var bd = new Date(startDate),be = new Date(endDate);
    var bd_time = bd.getTime(), be_time = be.getTime(),time_diff = be_time - bd_time;
    var d_arr = [];
    for(var i=0; i<= time_diff; i+=86400000){
        var ds = new Date(bd_time+i);
        d_arr.push(ds.getFullYear()+'-'+(ds.getMonth()+1)+'-'+ds.getDate()+'')
    }
    return d_arr;
}

/**
 * 获取从开始时间到结束时间中的所有月
 * 用法：
 *  1、byStartEndDateGetMiddleMonthDate("2020-1-1","2020-6-1")
 *  2、byStartEndDateGetMiddleMonthDate("2020-1","2020-6")
 * @param startDate
 * @param endDate
 * @returns {array}
 */
function byStartEndDateGetMiddleMonth(startDate,endDate) {
    var d1 = startDate;
    var d2 = endDate;
    var dateArry = new Array();
    var s1 = d1.split("-");
    var s2 = d2.split("-");
    var mCount = 0;
    if (parseInt(s1[0]) < parseInt(s2[0])) {
        mCount = (parseInt(s2[0]) - parseInt(s1[0])) * 12 + parseInt(s2[1]) - parseInt(s1[1])+1;
    } else {
        mCount = parseInt(s2[1]) - parseInt(s1[1])+1;
    }
    if (mCount > 0) {
        var startM = parseInt(s1[1]);
        var startY = parseInt(s1[0]);
        for (var i = 0; i < mCount; i++) {
            if (startM < 12) {
                dateArry[i] = startY + "-" + (startM>9 ? startM : "0" + startM);
                startM += 1;
            } else {
                dateArry[i] = startY + "-" + (startM > 9 ? startM : "0" + startM);
                startM = 1;
                startY += 1;
            }
        }
    }
    return dateArry;
}

/**
 * 根据开始结束日期获取中间的所有年
 * 用法：
 *  1、byStartEndDateGetMiddleYear("2020-1-1","2021-1-1")
 *  2、byStartEndDateGetMiddleYear("2020-1","2021-1")
 *  3、byStartEndDateGetMiddleYear("2020","2021")
 * @param startDate
 * @param endDate
 * @returns {any[]}
 */
function byStartEndDateGetMiddleYear(startDate,endDate) {
    var d1 = startDate;
    var d2 = endDate;
    var dateArry = new Array();
    var s1 = d1.split("-");
    var s2 = d2.split("-");
    var mYearCount = parseInt(s2[0]) - parseInt(s1[0])+1;
    var startY = parseInt(s1[0]);
    for (var i = 0; i < mYearCount;i++) {
        dateArry[i] = startY;
        startY += 1;
    }
    return dateArry;
};

/**
 * 根据指定日期，获取星期几
 * 用法：byDateGetWeek("2020-1-5")
 * @param dateString
 * @returns {string}
 */
function byDateGetWeek(dateString) {
    var dateArray = dateString.split("-");
    date = new Date(dateArray[0], parseInt(dateArray[1] - 1), dateArray[2]);
    return "周" + "日一二三四五六".charAt(date.getDay());
};


/**
 * 获取年的开始日期和结束日期
 * @param vars
 * @returns {string}
 */
function getYearStartEndDate(vars){
    var array = [];
    array[0] = vars+"-01-01";
    array[1] = vars+"-12-31";
    return array;
}

/**
 * 根据json字段排序
 * 用法：var jsonStr = jsonStr.sort(up);
 * @param x
 * @param y
 * @returns {number}
 */
//按照升序排列
function up(x,y){
    return y.COUNT-x.COUNT
}

/**
 * Fri Dec 12 2014 08:00:00 GMT+0800日期转换格式
 * 用法：formateDate("Fri Dec 12 2014 08:00:00 GMT+0800")
 * @param str
 * @returns {string}
 */
function formateDate(str){
    var formate = new Date(str);
    return formate.getFullYear() + '-' + (formate .getMonth() + 1) + '-' + formate .getDate() + ' ' + formate .getHours() + ':' + formate.getMinutes() + ':' + formate.getSeconds();
}

/**
 * 获得本页面地址栏参数
 * 用法：getUrlAddressParameter("name")
 * @param parameter
 * @returns {*}
 */
function getUrlAddressParameter(parameter){
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == parameter){return pair[1];}
    }
    return null;
}

/**
 * 获得Guid
 * 用法:getGuid()
 * @returns {string}
 */
function getGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

/**
 * 保存canvas为图片
 * 用法：exportCanvasAsPNG("文件名")
 * @param fileName
 */
function exportCanvasAsPNG(fileName) {
    var canvasElement = document.getElementsByTagName("canvas")[0]
    var MIME_TYPE = "image/png";
    var imgURL = canvasElement.toDataURL(MIME_TYPE);

    var dlLink = document.createElement('a');
    dlLink.download = fileName;
    dlLink.href = imgURL;
    dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');

    document.body.appendChild(dlLink);
    dlLink.click();
    document.body.removeChild(dlLink);
}


/**
 * 保存div为图片
 *
 * 该访问为外部方法，如需使用，请解开注解，并且添加js引用
 * js引用地址：<script src="https://cdn.bootcdn.net/ajax/libs/html2canvas/0.4.0/html2canvas.min.js"></script>
 */
/*
html2canvas($("#qrcodeCanvas"), {
    onrendered: function(canvas) {
        var MIME_TYPE = "image/png";
        var imgURL = canvas.toDataURL(MIME_TYPE);

        var dlLink = document.createElement('a');
        dlLink.download = "a"
        dlLink.href = imgURL;
        dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');

        document.body.appendChild(dlLink);
        dlLink.click();
        document.body.removeChild(dlLink);
        count = count + 1
    }
})*/
